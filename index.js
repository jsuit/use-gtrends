const gtrends = require('google-trends-api');

const NTIMES = 199;

const pAll = ','.repeat(NTIMES).split(',')
.map(() => gtrends.interestOverTime({
  keyword: 'peperos',
  category: 0
}));

Promise.all(pAll)
.then((results) => {
  console.log('These results are awesome', results.length);
  console.log(results[0]);
})
.catch((err) => {
  console.error('Oh no there was an error', err);
});
